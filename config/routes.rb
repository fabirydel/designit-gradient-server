Rails.application.routes.draw do
  namespace :api, defaults: { format: :json }  do
    namespace :v1 do
      resources :templates, only: [:create, :index, :destroy] 
    end
  end
  
  match '*all', controller: 'application', action: 'cors_preflight_check', via: [:options]
end
