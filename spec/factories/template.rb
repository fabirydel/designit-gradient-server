FactoryGirl.define do
  factory :template do
    name { Faker::Name.name }
    creator { Faker::Name.name }
    style 'radial'
    direction 'bottom'
    color_one '#74e3ec'
    color_two '#c7ffe2'
    color_format '#c7ffe2'
  end
end
