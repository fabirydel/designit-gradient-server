require 'spec_helper'

describe Template, type: :model do
  it { expect validate_presence_of(:name) }
  it { expect validate_presence_of(:creator) }
  it { expect validate_presence_of(:style) }
  it { expect validate_presence_of(:direction) }
  it { expect validate_presence_of(:color_one) }
  it { expect validate_presence_of(:color_two) }
  it { expect validate_presence_of(:color_format) }
  it { should validate_uniqueness_of(:name).scoped_to(:creator) }

  it 'has a valid factory' do
    expect(FactoryGirl.create(:template)).to be_valid
  end
end
