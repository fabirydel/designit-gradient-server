require 'spec_helper'

describe Api::V1::TemplatesController, type: :controller do
  render_views

  describe 'create' do
    context 'with valid params' do
      it 'creates template correctly' do
        expect{
          post :create, params: { template: FactoryGirl.attributes_for(:template) }, format: :json
        }.to change(Template, :count).by(1)
      end

      it 'should return 200' do
        post :create, params: { template: FactoryGirl.attributes_for(:template) }, format: :json
        expect(response.status).to eq(200)
      end
    end

    context 'with invalid params' do
      it 'does not create template' do
        expect{
          post :create, params: { template: FactoryGirl.attributes_for(:template, name: '') }, format: :json
        }.to_not change(Template, :count)
      end

      it 'Sets action errors on template variable' do
        post :create, params: { template: FactoryGirl.attributes_for(:template, name: '') }, format: :json
        expect(assigns(:template).errors.keys).to match_array([:name])
      end

      it 'should return 400' do
        post :create, params: { template: FactoryGirl.attributes_for(:template, name: '') }, format: :json
        expect(response.status).to eq(400)
      end
    end
  end

  describe 'destroy' do
    let!(:template) { FactoryGirl.create(:template) }
    subject { template }

    context 'with valid params' do
      it 'deletes template correctly' do
        expect{
          delete :destroy, params: { id: template.id }, format: :json
        }.to change(Template, :count).by(-1)
      end

      it 'should return 200' do
        delete :destroy, params: { id: template.id }, format: :json
        expect(response.status).to eq(200)
      end
    end
  end

  describe 'index' do
    let!(:templates) { FactoryGirl.create_list(:template, 2) }

    it 'should return 200' do
      get :index, format: :json
      expect(response.status).to eq(200)
    end

    it 'should populate templates with all the existing templates' do
      get :index, format: :json
      expect(assigns(:templates)).to match_array(templates)
    end

    it 'should return the right json object' do
      get :index, format: :json

      json = JSON.parse(response.body)
      expect(json).to have_key('templates')
      expect(json['templates'].size).to eq(2)
      expect(json['templates'][0]).to have_key('id')
      expect(json['templates'][0]).to have_key('name')
      expect(json['templates'][0]).to have_key('creator')
      expect(json['templates'][0]).to have_key('style')
      expect(json['templates'][0]).to have_key('direction')
      expect(json['templates'][0]).to have_key('color_one')
      expect(json['templates'][0]).to have_key('color_two')
      expect(json['templates'][0]).to have_key('color_format')
    end
  end
end
