class CreateTemplates < ActiveRecord::Migration[5.1]
  def change
    create_table :templates do |t|
      t.string :name
      t.string :creator
      t.string :style
      t.string :direction
      t.string :color_one
      t.string :color_two
      t.string :color_format
    end
  end
end
