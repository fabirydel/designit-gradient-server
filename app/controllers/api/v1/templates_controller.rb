module Api
  module V1
    class TemplatesController < ApplicationController
      before_action :load_template, only: [:update, :destroy]

      def create
        @template = Template.new(template_params)
        if @template.save
          render json: { }
        else
          render json: { errors: @template.errors }, status: :bad_request
        end
      end

      def index
        @templates = Template.all
      end


      def destroy
        if @template.destroy
          render json: { }
        else
          render json: { errors: @template.errors }, status: :bad_request
        end
      end

      private

      def load_template
        @template = Template.find(params[:id])
      end

      def template_params
        params.require(:template).permit(:name, :creator, :style, :direction, :color_one, 
          :color_two, :color_format)
      end
    end
  end
end
