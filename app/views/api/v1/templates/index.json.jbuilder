json.templates @templates do |template|
  json.id template.id
  json.name template.name
  json.creator template.creator
  json.style template.style
  json.direction template.direction
  json.color_one template.color_one
  json.color_two template.color_two
  json.color_format template.color_format
end
