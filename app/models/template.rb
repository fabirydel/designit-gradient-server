class Template < ApplicationRecord
  validates_presence_of :name, :creator, :style, :direction, :color_one, :color_two, :color_format
  validates :name, uniqueness: { scope: :creator }
end
